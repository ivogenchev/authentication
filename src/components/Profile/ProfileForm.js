import { useRef, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import AuthContext from '../../store/auth-context';
import classes from './ProfileForm.module.css';


const ProfileForm = () => {
  const history = useHistory()
  const authCtx = useContext(AuthContext)
  const getNewPassRef = useRef();
  const submitHandler = (ev) => {
    ev.preventDefault();
    const newPassword = getNewPassRef.current.value;

    fetch("https://identitytoolkit.googleapis.com/v1/accounts:update?key=AIzaSyB4GMekJwaudFJ79HsouD7T4Q6JcZAgXd0",{
      method: 'POST',
      headers: {'Content-type': "Application/json"},
      body: JSON.stringify({
        idToken: authCtx.token,
        password: newPassword,
        returnSecureToken: false,
      })
    }).then(res => {
      // assumption: Always succeeds!
      history.replace('/');
    }) 
  }


  return (
    <form className={classes.form} onSubmit={submitHandler}>
      <div className={classes.control}>
        <label htmlFor='new-password'>New Password</label>
        <input type='password' id='new-password' ref={getNewPassRef}/>
      </div>
      <div className={classes.action}>
        <button>Change Password</button>
      </div>
    </form>
  );
}

export default ProfileForm;
